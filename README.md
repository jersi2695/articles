#Run Application

1. Build Docker image with the following command
    ```docker build -t articles:1.0 .```
2. Up docker compose
    ```docker compose up```
3. Now the application is available in 8080 port


#Use Application

##APIs

GET /articles

Params:
1. size (max=5), the amount of articles that will be in the page
2. page, the page is wanted to get, it starts from 0
3. filter, the field is wanted to filter the articles, the allowed values are [AUTHOR,TAG,TITLE,MONTH]
4. filterValue, the value the field must match to filter the articles


DELETE /articles/{objectId}

This api performs a logical deletion of the article to avoid the job get the article again.