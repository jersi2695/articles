package com.applydigital.articles.services;


import com.applydigital.articles.enums.FilterEnum;
import com.applydigital.articles.models.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface IArticleService
{
    Page<Article> getArticles( Pageable pageable );

    Page<Article> getArticles( Pageable pageable, FilterEnum filter, String filterValue );

    void deleteArticle( String objectId );
}
