package com.applydigital.articles.services;

import com.applydigital.articles.enums.FilterEnum;
import com.applydigital.articles.models.Article;
import com.applydigital.articles.repositories.ArticleRepository;
import lombok.AllArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.Month;


@Service
@AllArgsConstructor
public class ArticleService implements IArticleService
{

    private final ArticleRepository articleRepository;

    @Override
    public Page<Article> getArticles( final Pageable pageable )
    {
        return articleRepository.findAll(pageable);
    }


    @Override
    public Page<Article> getArticles( final Pageable pageable,
                                      final FilterEnum filter,
                                      final String filterValue )
    {
        return switch ( filter ){
            case AUTHOR -> articleRepository.findArticlesByAuthorWithPagination( pageable, filterValue );
            case MONTH ->  articleRepository.findArticlesByMonthWithPagination( pageable, Month.valueOf( filterValue.toUpperCase() ).getValue() );
            case TITLE -> articleRepository.findArticlesByTitleWithPagination( pageable, filterValue );
            case TAG -> articleRepository.findArticlesByTagWithPagination( pageable, filterValue );
        };
    }


    @Override
    public void deleteArticle( final String objectId ) throws ObjectNotFoundException
    {
        Article article = articleRepository.findById( objectId ).orElseThrow(
              () -> new ObjectNotFoundException( objectId, "article" ));
        article.setDeleted( true );
        article.setDeletedAt( LocalDateTime.now() );
        articleRepository.save( article );
    }
}
