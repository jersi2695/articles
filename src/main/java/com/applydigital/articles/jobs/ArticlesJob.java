package com.applydigital.articles.jobs;

import com.applydigital.articles.clients.SearchClient;
import com.applydigital.articles.dtos.NewsDTO;
import com.applydigital.articles.mappers.HitToArticleMapper;
import com.applydigital.articles.repositories.ArticleRepository;
import lombok.AllArgsConstructor;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;


@Component
@AllArgsConstructor
public class ArticlesJob implements Job
{

    private final SearchClient searchClient;
    private final ArticleRepository articleRepository;
    private final HitToArticleMapper mapper;

    @Override
    public void execute( final JobExecutionContext jobExecutionContext )
          throws JobExecutionException
    {
        NewsDTO newsDTO = searchClient.searchNewsByDate();
        newsDTO.getHits().stream()
               .map(mapper::hitToArticle)
               .filter( a ->  !articleRepository.existsById( a.getObjectID() ))
               .forEach(articleRepository::save);
    }





}
