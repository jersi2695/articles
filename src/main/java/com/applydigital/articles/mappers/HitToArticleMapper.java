package com.applydigital.articles.mappers;

import com.applydigital.articles.dtos.HitDTO;
import com.applydigital.articles.models.Article;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring")
public interface HitToArticleMapper
{
    Article hitToArticle( HitDTO hit );
}
