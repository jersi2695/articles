
package com.applydigital.articles.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@JsonIgnoreProperties
public class HitDTO
{
    @JsonProperty("_tags")
    private List<String> tags;
    private String author;
    @JsonProperty("comment_text")
    private String commentText;
    @JsonProperty("created_at")
    private LocalDateTime createdAt;
    @JsonProperty("created_at_i")
    private Long createdAtI;
    @JsonProperty("num_comments")
    private Long numComments;
    private String objectID;
    @JsonProperty("parent_id")
    private Long parentId;
    private Long points;
    @JsonProperty("story_id")
    private Long storyId;
    @JsonProperty("story_text")
    private String storyText;
    @JsonProperty("story_title")
    private String storyTitle;
    @JsonProperty("story_url")
    private String storyUrl;
    private String title;
    private String url;

}
