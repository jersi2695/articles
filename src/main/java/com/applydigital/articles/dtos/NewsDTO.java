
package com.applydigital.articles.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties
public class NewsDTO {

    private List<HitDTO> hits;

}
