package com.applydigital.articles.enums;


public enum FilterEnum
{
    AUTHOR("author"),
    TAG("tag"),
    TITLE("title"),
    MONTH("month");

    private final String text;

    FilterEnum(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
