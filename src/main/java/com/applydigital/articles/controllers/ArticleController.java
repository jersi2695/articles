package com.applydigital.articles.controllers;

import com.applydigital.articles.enums.FilterEnum;
import com.applydigital.articles.models.Article;
import com.applydigital.articles.repositories.ArticleRepository;
import com.applydigital.articles.services.IArticleService;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.ObjectNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@AllArgsConstructor
@RequestMapping("/articles")
public class ArticleController
{

    private final IArticleService articleService;

    @GetMapping
    public ResponseEntity<Page<Article>> getArticles( Pageable pageable, @RequestParam(required = false) String filter, @RequestParam(required = false) String filterValue){
        if( StringUtils.isBlank( filter )  && StringUtils.isBlank( filterValue )){
            return ResponseEntity.ok(articleService.getArticles( pageable ));
        }else if( !StringUtils.isBlank( filter )  && !StringUtils.isBlank( filterValue )){
            try{
                FilterEnum filterEnum = FilterEnum.valueOf( filter );
                return ResponseEntity.ok(articleService.getArticles( pageable, filterEnum, filterValue ));
            }catch ( IllegalArgumentException  ex){
                return ResponseEntity.badRequest().build();
            }
        }else{
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/{articleId}")
    public ResponseEntity deleteArticle( @PathVariable("articleId") String articleId ){
        try
        {
            articleService.deleteArticle( articleId );
            return ResponseEntity.ok().build();
        }catch ( ObjectNotFoundException ex ){
            return ResponseEntity.notFound().build();
        }
    }


}
