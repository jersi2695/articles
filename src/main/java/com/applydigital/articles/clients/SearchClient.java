package com.applydigital.articles.clients;

import com.applydigital.articles.dtos.NewsDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@FeignClient(name = "searchClient", url = "https://hn.algolia.com/api/v1")
public interface SearchClient
{

    @RequestMapping(method = RequestMethod.GET, value = "/search_by_date?query=java")
    NewsDTO searchNewsByDate();

}
