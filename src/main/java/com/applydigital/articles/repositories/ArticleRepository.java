package com.applydigital.articles.repositories;


import com.applydigital.articles.models.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ArticleRepository extends PagingAndSortingRepository<Article, String>
{
    @Query("SELECT a FROM Article a WHERE a.deleted = False")
    Page<Article> findAllArticlesWithPagination( Pageable pageable );

    @Query("SELECT a FROM Article a WHERE a.deleted = False AND a.author LIKE %:author%")
    Page<Article> findArticlesByAuthorWithPagination( Pageable pageable, @Param( "author" ) String author );

    @Query("SELECT a FROM Article a WHERE a.deleted = False AND array_to_string(a.tags, ',') LIKE %:tag%")
    Page<Article> findArticlesByTagWithPagination( Pageable pageable, @Param( "tag" ) String tag );

    @Query("SELECT a FROM Article a WHERE a.deleted = False AND a.storyTitle LIKE %:title%")
    Page<Article> findArticlesByTitleWithPagination( Pageable pageable, @Param( "title" ) String title );

    @Query("SELECT a FROM Article a WHERE a.deleted = False AND month(a.createdAt) = :month")
    Page<Article> findArticlesByMonthWithPagination( Pageable pageable, @Param( "month" ) Integer month );

    @Query("SELECT a FROM Article a WHERE a.deleted = False AND a.objectID = :objectId ")
    Optional<Article> findById( @Param( "objectId" ) String objectId );
}
