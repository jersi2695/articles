package com.applydigital.articles.config;


import org.hibernate.dialect.PostgreSQL94Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;


public class CustomDialect extends PostgreSQL94Dialect
{
    public CustomDialect() {
        super();
        registerFunction("array_to_string", new StandardSQLFunction( "array_to_string"));
    }
}
