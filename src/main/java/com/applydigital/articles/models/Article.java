
package com.applydigital.articles.models;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@TypeDef(
      name = "list-array",
      typeClass = ListArrayType.class
)
public class Article
{
    @Id
    private String objectID;
    @Type( type = "list-array" )
    @Column(columnDefinition = "text[]")
    private List<String> tags;
    private String author;
    @Column(columnDefinition = "text")
    private String commentText;
    private LocalDateTime createdAt;
    private Long createdAtI;
    private Long numComments;
    private Long parentId;
    private Long points;
    private Long storyId;
    private String storyText;
    private String storyTitle;
    private String storyUrl;
    private String title;
    private String url;
    @Column(nullable = false)
    private Boolean deleted = false;
    private LocalDateTime deletedAt;

}
