package com.applydigital.articles.controllers;

import com.applydigital.articles.enums.FilterEnum;
import com.applydigital.articles.services.IArticleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith( SpringExtension.class )
@SpringBootTest
public class ArticleControllerTest
{
    @MockBean
    IArticleService articleService;

    @Autowired
    ArticleController articleController;

    @Test
    void getArticlesFailBadRequestMissingParameterFilterValue(){
        ResponseEntity response = articleController.getArticles( Pageable.unpaged(), FilterEnum.TAG.toString(), null );
        assertEquals( HttpStatus.BAD_REQUEST , response.getStatusCode());
    }

    @Test
    void getArticlesFailBadRequestMissingParameterFilter(){
        ResponseEntity response = articleController.getArticles( Pageable.unpaged(), null, "xhector" );
        assertEquals( HttpStatus.BAD_REQUEST , response.getStatusCode());
    }

    @Test
    void getArticlesFailBadRequestBadFilter(){
        ResponseEntity response = articleController.getArticles( Pageable.unpaged(), "CREATED_DATE", "xhector" );
        assertEquals( HttpStatus.BAD_REQUEST , response.getStatusCode());
    }

    @Test
    void getArticlesSuccess(){
        ResponseEntity response = articleController.getArticles( Pageable.unpaged(), null, null );
        assertEquals( HttpStatus.OK , response.getStatusCode());
        verify( articleService, times(1)).getArticles( any() );
    }

    @Test
    void getArticlesWithFilterSuccess(){
        ResponseEntity response = articleController.getArticles( Pageable.unpaged(), "AUTHOR", "xhector" );
        assertEquals( HttpStatus.OK , response.getStatusCode());
        verify( articleService, times(1)).getArticles( any(), any(), anyString() );
    }
}
