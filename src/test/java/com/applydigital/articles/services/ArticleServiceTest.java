package com.applydigital.articles.services;

import com.applydigital.articles.enums.FilterEnum;
import com.applydigital.articles.models.Article;
import com.applydigital.articles.repositories.ArticleRepository;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@ExtendWith( SpringExtension.class )
@SpringBootTest
public class ArticleServiceTest
{
    @MockBean
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleService articleService;

    @Test
    void getArticlesSuccess(){
        when(articleRepository.findAll( (Pageable) any() )).thenReturn( mockFindAll() );
        Page<Article> articles = articleService.getArticles( Pageable.unpaged() );
        assertNotNull( articles );
        assertEquals(2, articles.getTotalElements());
    }

    @Test
    void getArticlesByAuthorSuccess(){
        articleService.getArticles( Pageable.unpaged(), FilterEnum.AUTHOR, "xhector" );
        verify( articleRepository, times(1) ).findArticlesByAuthorWithPagination( any(), eq( "xhector" ) );
    }

    @Test
    void getArticlesByMonthSuccess(){
        articleService.getArticles( Pageable.unpaged(), FilterEnum.MONTH, "OCTOBER" );
        verify( articleRepository, times(1) ).findArticlesByMonthWithPagination( any(), eq( 10 ) );
    }

    @Test
    void getArticlesByTitleSuccess(){
        articleService.getArticles( Pageable.unpaged(), FilterEnum.TITLE, "Java Generics" );
        verify( articleRepository, times(1) ).findArticlesByTitleWithPagination( any(), eq( "Java Generics" ) );
    }

    @Test
    void getArticlesByTagSuccess(){
        articleService.getArticles( Pageable.unpaged(), FilterEnum.TAG, "JAVA" );
        verify( articleRepository, times(1) ).findArticlesByTagWithPagination( any(), eq( "JAVA" ) );
    }

    @Test
    void deleteArticleFail(){
        when( articleRepository.findById( anyString() ) ).thenReturn( Optional.empty() );
        assertThrows( ObjectNotFoundException.class, () -> articleService.deleteArticle( "1223423" ) );
    }

    @Test
    void deleteArticleSuccess(){
        when( articleRepository.findById( anyString() ) ).thenReturn( Optional.of(new Article()) );
        articleService.deleteArticle( "1223423" );
        verify( articleRepository, times(1) ).save( any() );
    }


    Page<Article> mockFindAll(){
        Article article1 = new Article();
        article1.setAuthor( "xhector" );
        article1.setStoryText( "Lorem ipsum" );

        Article article2 = new Article();
        article2.setAuthor( "hSmith" );
        article2.setStoryText( "Lorem ipsum 12434" );

        return new PageImpl<>(List.of(article1, article2));
    }
}
