package com.applydigital.articles.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;


@ExtendWith( SpringExtension.class )
@SpringBootTest
public class JwtUserDetailsServiceTest
{

    @Autowired
    JwtUserDetailsService jwtUserDetailsService;

    @Test
    void loadUserSuccess(){
        UserDetails user = jwtUserDetailsService.loadUserByUsername( "applydigital" );
        assertNotNull(user);
        assertEquals( "applydigital", user.getUsername() );
    }

    @Test
    void loadUserFail(){
        assertThrows( UsernameNotFoundException.class, () -> jwtUserDetailsService.loadUserByUsername( "aaaa" ));
    }
}
